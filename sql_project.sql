---we will use 2019 data---
--questions to answer:
--duration of the trip
--most popular stations at the begining and end of the day
--At the most popular locations do we have more single time user vs subscribers

select * from divvy_stations
select * from distances
select * from distances_km
select * from divvybikes_2018



--most poppular = most trips
--join divvy_stations to divvybikes_2018
SELECT start_time, end_time, CAST((end_time - start_time) AS ) AS avgtimeused
FROM divvybikes_2019
GROUP BY start_time, end_time;

SELECT (DATE_PART(hour, (end_time - start_time) ) * 3600) + (DATE_PART(minute, (end_time - start_time) ) * 60) + DATEPART(second, (end_time - start_time)) as SecondsFromMidnight FROM T1

SELECT * , CAST(start_time as time), CAST(end_time as time)
FROM divvybikes_2019
WHERE start_time::time > '06:59:00'::TIME AND start_time::time < '09:30:00';

Select count(user_type) from divvybikes_2019
where user_type ilike 'Subscriber'


Select count(user_type) from divvybikes_2019
where user_type ilike 'customer'

Select count(user_type) from divvybikes_2019
Select user_type from divvybikes_2019 where user_type is not null

--top 10 morning stations
select divvybikes_2019.start_station_id, divvy_stations.name,COUNT(divvybikes_2019.start_station_id)
from divvybikes_2019
JOIN divvy_stations on divvybikes_2019.start_station_id = divvy_stations.id
WHERE start_time::time > '07:00:00'::TIME AND start_time::time < '09:30:00'
GROUP BY divvy_stations.name,divvybikes_2019.start_station_id 
ORDER BY COUNT(divvybikes_2019.start_station_id) DESC
LIMIT 10;

-- top 10 end of day stations
select divvybikes_2019.end_station_id, divvy_stations.name,COUNT(divvybikes_2019.end_station_id)
from divvybikes_2019
JOIN divvy_stations on divvybikes_2019.end_station_id = divvy_stations.id
WHERE end_time::time > '19:00:00'::TIME AND end_time::time < '21:30:00'
GROUP BY divvy_stations.name,divvybikes_2019.end_station_id 
ORDER BY COUNT(divvybikes_2019.end_station_id) DESC
LIMIT 10;

-- profit from subscribers
select count(distinct(bikeid)) from divvybikes_2019

select (count(user_type)/4)*119 from divvybikes_2019 
where user_type ilike 'Subscriber'

select count(user_type)*24*0.16 from divvybikes_2019 
where user_type ilike 'Customer'

